const fs = require('fs');
const Mastodon = require('mastodon');

const M = new Mastodon({
  access_token: process.env.MASTODON_ACCESS_TOKEN,
  api_url: process.env.MASTODON_API,
});

module.exports = {
  async postImg(img) {
    const media = await M.post('media', {
      file: fs.createReadStream(img),
    });
    return M.post('statuses', {
      status: '',
      media_ids: [media.data.id],
    });
  },
};
