# mastoframerbot

Posts a random frame from a random video found in certain folder to mastodon.

For now this is just a little experiment, and it's specifically tailored to my actual needs.

## Requirements

* FFMpeg installed  
* mastodon api key  
* some videos  

## Usage
```
Options:
  --version     Show version number                                    [boolean]
  --folder, -f  folder containing the videos                          [required]
  --output, -o  output folder where the screenshots will be saved     [required]
  -h, --help    Show help                                              [boolean]
```
A single execution posts a random screenshot. You can keep it running periodically with cron jobs or similar.