#!/usr/bin/env node

require('dotenv').config();
const util = require('util');
const glob = util.promisify(require('glob'));
const sample = require('lodash.sample');

const { extractFrame } = require('./framer');
const { postImg } = require('./masto');

// eslint-disable-next-line import/order
const { argv } = require('yargs')
  .option('folder', {
    alias: 'f',
    describe: 'folder containing the videos',
    demandOption: true,
  })
  .option('output', {
    alias: 'o',
    describe: 'output folder where the screenshots will be saved',
    demandOption: true,
  })
  .help('h')
  .alias('h', 'help');

const main = async () => {
  const files = await glob(`${argv.folder}/**/*.@(mp4|mkv)`);
  const f = sample(files);
  const frame = await extractFrame(f, argv.output);
  const postResult = await postImg(`${argv.output}/${frame}`);
  console.log(`toot sent: ${postResult.data.uri}`);
};

main();
