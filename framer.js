const ffmpeg = require('fluent-ffmpeg');
const { exec } = require('child_process');

const getRandomTimestamp = (maxMs) => {
  const ms = Math.floor(Math.random() * maxMs);
  return ms / 1000;
};

const getDuration = file => new Promise((resolve, reject) => {
  exec(`ffprobe -v quiet -print_format json -show_format -show_streams "${file}"`, (err, stdout) => {
    if (err) {
      return reject(err);
    }
    try {
      const data = JSON.parse(stdout);
      return resolve(parseFloat(data.format.duration) * 1000);
    } catch (error) {
      return reject(error);
    }
  });
});

const extractFrame = (file, output) => getDuration(file)
  .then(duration => new Promise((resolve, reject) => {
    const outputFilename = `${(new Date()).toISOString().replace(/[-,:,\.]/g, '')}.png`;
    ffmpeg(file)
      .on('end', () => resolve(outputFilename))
      .on('error', err => reject(err))
      .screenshots({
        timestamps: [getRandomTimestamp(duration)],
        filename: `${outputFilename}`,
        folder: output,
      });
  }));

module.exports = {
  extractFrame,
};
